# PhotoSearch

An android app, which is connecting to unsplash services to retrieve photos.

## A bit of info for who's reading

Hi, this is the photo search project. This app allow to search photos uploaded on Unsplash site.
I attached all the useful documentation on **/documentation** folder with mockups and the final apk.

Have a good day!